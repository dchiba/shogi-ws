package playerid

import "sync/atomic"

var maxId uint64 = 0

func NewPlayerId() uint64 {
	return atomic.AddUint64(&maxId, 1)
}
