package game

import (
	"bitbucket.org/dchiba/shogi/player"
	"bitbucket.org/dchiba/shogi/shogi"
)

type Game struct {
	Shogi           *shogi.Shogi
	initializedList []bool
}

func NewGame(mode shogi.Mode) *Game {
	return &Game{
		Shogi:           shogi.New(mode),
		initializedList: make([]bool, 2, 2),
	}
}

func (g *Game) Status() shogi.Status {
	return g.Shogi.Status
}

func (g *Game) SetPlayer(p *player.Player) error {
	if len(g.Shogi.Players()) == 0 {
		p.Sente = true
	} else {
		p.Sente = false
	}
	err := g.Shogi.SetPlayer(p)
	if err != nil {
		return err
	}
	return nil
}

func (g *Game) AddInitializedPlayer(p *player.Player) {
	if p.ID == g.Shogi.Player1.ID {
		g.initializedList[0] = true
	} else {
		g.initializedList[1] = true
	}
}

func (g *Game) ClientInitialized() bool {
	count := 0
	for _, b := range g.initializedList {
		if b {
			count++
		}
	}
	return count == 2
}

func Int2Mode(i int) shogi.Mode {
	switch i {
	case 0:
		return shogi.DefaultMode
	case 1:
		return shogi.MiniMode
	case 2:
		return shogi.MicroMode
	default:
		panic("invalid mode")
	}
}
