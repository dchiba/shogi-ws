package connection

import (
	"bitbucket.org/dchiba/shogi-ws/app/model/game"
	"github.com/stretchr/testify/assert"
	"github.com/trevex/golem"
	"testing"
)

func TestConnection_GetGame(t *testing.T) {
	conn1 := NewConnection(&golem.Connection{})
	conn1.roomName = "room1"
	conn2 := NewConnection(&golem.Connection{})
	conn2.roomName = "room2"
	conn3 := NewConnection(&golem.Connection{})
	conn3.roomName = "room1"

	g := game.NewGame(game.Int2Mode(0))

	conn1.SetGame(g)
	g2, ok := conn1.GetGame()

	assert.True(t, ok, "game が取得できている")
	assert.NotNil(t, g2)

	g3, ok := conn2.GetGame()

	assert.False(t, ok, "部屋違いなので game が取得できない")
	assert.Nil(t, g3)

	g4, ok := conn3.GetGame()

	assert.True(t, ok, "conn1 と同じ部屋なので game が取得できている")
	assert.NotNil(t, g4)
}
