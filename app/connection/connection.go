package connection

import (
	"bitbucket.org/dchiba/shogi-ws/app/model/game"
	"bitbucket.org/dchiba/shogi/player"
	"github.com/trevex/golem"
	"log"
)

var (
	roomManager = golem.NewRoomManager()
	roomStorage = NewRoomStorage()

	RandomJoinDefault = -1

	gameKey = "game"
)

type Emiter interface {
	Emit(string, interface{})
}

type RoomEmiter interface {
	Emit(string, string, interface{})
}

type Connection struct {
	*golem.Connection
	roomName   string
	Player     *player.Player
	RandomJoin int
	Emiter     Emiter
	RoomEmiter RoomEmiter
}

func NewConnection(conn *golem.Connection) *Connection {
	return &Connection{
		Connection: conn,
		RandomJoin: RandomJoinDefault,
		Emiter:     conn,
		RoomEmiter: roomManager,
	}
}

func (conn *Connection) Emit(event string, data interface{}) {
	conn.Emiter.Emit(event, data)
}

func (conn *Connection) Join(name string) {
	conn.roomName = name
	log.Println("Join room: " + name)
	roomManager.Join(name, conn.Connection)
}

func (conn *Connection) RoomEmit(event string, data interface{}) {
	if conn.roomName != "" {
		conn.RoomEmiter.Emit(conn.roomName, event, data)
	}
}

func (conn *Connection) LeaveAll() {
	roomManager.LeaveAll(conn.Connection)
}

func (conn *Connection) GetGame() (*game.Game, bool) {
	if data, ok := roomStorage.Get(conn.roomName, gameKey); ok {
		return data.(*game.Game), true
	}
	return nil, false
}

func (conn *Connection) SetGame(g *game.Game) {
	roomStorage.Set(conn.roomName, gameKey, g)
}

type RoomStorage struct {
	storage map[string]map[string]interface{}
}

func NewRoomStorage() *RoomStorage {
	return &RoomStorage{storage: make(map[string]map[string]interface{})}
}

func (rs *RoomStorage) Get(name string, key string) (interface{}, bool) {
	if roomData, ok := rs.storage[name]; ok {
		if data, ok := roomData[key]; ok {
			return data, true
		}
	}
	return nil, false
}

func (rs *RoomStorage) Set(name string, key string, data interface{}) {
	if roomData, ok := rs.storage[name]; ok {
		roomData[key] = data
		rs.storage[name] = roomData
	} else {
		rs.storage[name] = map[string]interface{}{key: data}
	}
}
