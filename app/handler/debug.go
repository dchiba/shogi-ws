package handler

import (
	"bitbucket.org/dchiba/shogi-ws/app/connection"
	"fmt"
	"time"
)

func DebugSetTime(conn *connection.Connection, data *DebugSetTimeRequest) {
	fmt.Printf("Debug Set Time: %d\n", data.Time)
	if g, ok := conn.GetGame(); ok {
		g.Shogi.SetApptime(time.Unix(data.Time, 0))
	}
}
