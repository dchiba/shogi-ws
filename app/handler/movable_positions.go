package handler

import (
	"bitbucket.org/dchiba/shogi-ws/app/connection"
)

func MovablePositions(conn *connection.Connection, data *MovablePositionsRequest) {
	g, ok := conn.GetGame()
	if !ok {
		conn.Emit("error", &ErrorResponse{Msg: "game is not exist"})
	}
	p := conn.Player

	position, err := g.Shogi.Ban.NewPosition(data.Position.X, data.Position.Y)
	if err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}
	positions, err := g.Shogi.MovablePositions(p, position)
	if err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}
	movePositions := []MovePositionInfo{}
	for _, pos := range positions {
		movePositions = append(movePositions, MovePositionInfo{
			Position: PositionInfo{X: pos.Position.X, Y: pos.Position.Y},
			Nari:     pos.Nari,
		})
	}
	conn.Emit("movable_positions", &MovablePositionResponse{
		Positions: movePositions,
	})
}
