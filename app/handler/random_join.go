package handler

import (
	"bitbucket.org/dchiba/shogi-ws/app/connection"
	"math/rand"
	"strconv"
	"sync"
	"time"
)

var joinQueueMap = map[int][]*connection.Connection{}

func RandomJoin(conn *connection.Connection, data *RandomJoinRequest) {
	mu := sync.RWMutex{}
	mu.Lock()
	joinQueue, ok := joinQueueMap[data.Mode]
	if !ok {
		joinQueue = []*connection.Connection{}
	}
	if len(joinQueue) > 0 && !Contains(joinQueue, conn) {
		index := rand.Intn(len(joinQueue))
		conn2 := joinQueue[index]
		joinQueue = append(joinQueue[:index], joinQueue[index+1:]...)
		joinQueueMap[data.Mode] = joinQueue
		name := strconv.FormatInt(time.Now().UnixNano(), 10)
		mu.Unlock()
		Join(conn, &JoinRequest{Name: name, Mode: data.Mode})
		Join(conn2, &JoinRequest{Name: name, Mode: data.Mode})
	} else {
		// 待ち行列に追加
		joinQueue = append(joinQueue, conn)
		joinQueueMap[data.Mode] = joinQueue
		mu.Unlock()
		// RandomJoin を待っていることを記録
		conn.RandomJoin = data.Mode
	}
}

func Contains(joinQueue []*connection.Connection, conn *connection.Connection) bool {
	for _, q := range joinQueue {
		if q == conn {
			return true
		}
	}
	return false
}

func RandomJoinClear(conn *connection.Connection, mode int) {
	mu := sync.RWMutex{}
	mu.Lock()
	joinQueue, ok := joinQueueMap[mode]
	if !ok || !Contains(joinQueue, conn) {
		return
	}
	var index int
	for i, c := range joinQueue {
		if c == conn {
			index = i
		}
	}
	joinQueue = append(joinQueue[:index], joinQueue[index+1:]...)
	joinQueueMap[mode] = joinQueue
}
