package handler

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestInitialized(t *testing.T) {
	roomid := "initializetest"

	// 一人目作成
	conn := NewTestContexts()
	Join(conn, &JoinRequest{Name: roomid})

	// 二人目作成
	conn2 := NewTestContexts()
	Join(conn2, &JoinRequest{Name: roomid})

	g, _ := conn.GetGame()
	assert.False(t, g.ClientInitialized(), "not initialized")

	Initialized(conn, &EmptyRequest{})

	g, _ = conn.GetGame()
	assert.False(t, g.ClientInitialized(), "not initialized")

	Initialized(conn2, &EmptyRequest{})

	g, _ = conn.GetGame()
	assert.True(t, g.ClientInitialized(), "initialized")

	assert.Equal(t, conn2.RoomEmiter.(*TestRoomEmiter).Msg, "start")
}
