package handler

import (
	"bitbucket.org/dchiba/shogi/shogi"
	"github.com/stretchr/testify/assert"
	"log"
	"testing"
)

func TestRandomJoin(t *testing.T) {

	// 一人目作成
	conn := NewTestContexts()

	// 一人目参加
	RandomJoin(conn, &RandomJoinRequest{Mode: 0})

	// 二人目作成
	conn2 := NewTestContexts()

	// 二人目参加
	RandomJoin(conn2, &RandomJoinRequest{Mode: 0})

	assert.Equal(t, conn.Emiter.(*TestEmiter).Msg, "join", "receive join message")
	assert.Equal(t, conn2.Emiter.(*TestEmiter).Msg, "join", "receive join message")

	myroomid := conn.RoomEmiter.(*TestRoomEmiter).Room
	assert.NotNil(t, myroomid, "入室した")
	log.Println(myroomid)

	g, _ := conn.GetGame()
	assert.NotNil(t, g, "game created")
	assert.Equal(t, len(g.Shogi.Players()), 2, "player created")

	assert.Equal(t, conn.RoomEmiter.(*TestRoomEmiter).Msg, "initialize", "send init message")
	assert.NotNil(t, conn.RoomEmiter.(*TestRoomEmiter).Data, "メンバーが揃って初期化通知は発行された")
}

func TestRandomJoin2(t *testing.T) {
	// ユーザー作成
	conn1 := NewTestContexts()
	conn2 := NewTestContexts()
	conn3 := NewTestContexts()

	// ユーザー 1, 2 がモード違いで参加
	RandomJoin(conn1, &RandomJoinRequest{Mode: 0})
	RandomJoin(conn2, &RandomJoinRequest{Mode: 1})

	assert.Nil(t, conn1.Emiter.(*TestEmiter).Data, "モード違いのため JoinResponse は受信しない")
	assert.Nil(t, conn2.Emiter.(*TestEmiter).Data, "モード違いのため JoinResponse は受信しない")

	// ユーザー 3 がユーザー 2 と同じモードで参加
	RandomJoin(conn3, &RandomJoinRequest{Mode: 1})

	assert.Equal(t, conn2.Emiter.(*TestEmiter).Msg, "join", "receive join message")
	assert.Equal(t, conn3.Emiter.(*TestEmiter).Msg, "join", "receive join message")

	myroomid := conn2.RoomEmiter.(*TestRoomEmiter).Room
	assert.NotNil(t, myroomid, "入室した")
	log.Println(myroomid)

	g, _ := conn2.GetGame()
	assert.NotNil(t, g, "game created")
	assert.Equal(t, len(g.Shogi.Players()), 2, "player created")
	assert.Equal(t, g.Shogi.Mode, shogi.MiniMode, "参加したモードになっている")

	assert.Equal(t, conn2.RoomEmiter.(*TestRoomEmiter).Msg, "initialize", "send init message")
	assert.NotNil(t, conn2.RoomEmiter.(*TestRoomEmiter).Data, "メンバーが揃って初期化通知は発行された")
}

func TestRandomJoinClear(t *testing.T) {
	// ユーザー作成
	conn1 := NewTestContexts()
	conn2 := NewTestContexts()
	conn3 := NewTestContexts()

	mode := 2

	// ユーザー 1 が参加
	RandomJoin(conn1, &RandomJoinRequest{Mode: mode})

	assert.Equal(t, len(joinQueueMap[mode]), 1, "ユーザー1が待っている")

	RandomJoinClear(conn1, mode)

	assert.Equal(t, len(joinQueueMap[mode]), 0, "ユーザー1のまち情報が消えている")

	// ユーザー 2, 3 が正常にゲームを開始できる
	RandomJoin(conn2, &RandomJoinRequest{Mode: mode})
	RandomJoin(conn3, &RandomJoinRequest{Mode: mode})

	assert.Equal(t, conn2.RoomEmiter.(*TestRoomEmiter).Msg, "initialize", "ゲームが開始された")
	assert.NotNil(t, conn2.RoomEmiter.(*TestRoomEmiter).Data, "メンバーが揃って初期化通知は発行された")
}
