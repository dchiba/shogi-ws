package handler

import (
	"bitbucket.org/dchiba/shogi-ws/app/connection"
	"bitbucket.org/dchiba/shogi-ws/app/model/game"
	"bitbucket.org/dchiba/shogi-ws/app/model/playerid"
	"bitbucket.org/dchiba/shogi/player"
	"fmt"
)

func Join(conn *connection.Connection, data *JoinRequest) {
	fmt.Println("Joining", data.Name)
	// room の作成
	conn.Join(data.Name)
	// ゲームの作成
	g, ok := conn.GetGame()
	if ok {
		if g.Shogi.Mode != game.Int2Mode(data.Mode) {
			conn.Emit("error", &ErrorResponse{Msg: "invalid mode"})
		}
	} else {
		g = game.NewGame(game.Int2Mode(data.Mode))
		conn.SetGame(g)
	}

	// プレイヤーの作成
	p := player.New(playerid.NewPlayerId(), true)
	if err := g.SetPlayer(p); err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}
	conn.Player = p
	// プレイヤーの通知
	conn.Emit("join", &JoinResponse{Player: NewPlayerInfo(p)})

	// 開始通知
	if g.Shogi.Prepared() {
		g.Shogi.Initialize()
		conn.RoomEmit("initialize", &InitializeResponse{
			Mode:   int(g.Shogi.Mode),
			Width:  g.Shogi.Ban.Width,
			Height: g.Shogi.Ban.Height,
			Players: []PlayerInfo{
				NewPlayerInfo(g.Shogi.Player1),
				NewPlayerInfo(g.Shogi.Player2),
			},
			State: NewStateInfo(g),
		})
	}
}
