package handler

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMove(t *testing.T) {
	roomid := "movetest"

	// プレイヤー参加
	conn := NewTestContexts()
	Join(conn, &JoinRequest{Name: roomid})
	resJoin := conn.Emiter.(*TestEmiter).Data.(*JoinResponse)
	player1 := resJoin.Player
	conn2 := NewTestContexts()
	Join(conn2, &JoinRequest{Name: roomid})
	resJoin = conn2.Emiter.(*TestEmiter).Data.(*JoinResponse)
	player2 := resJoin.Player

	// 初期化終了
	Initialized(conn, &EmptyRequest{})
	Initialized(conn2, &EmptyRequest{})

	Move(conn, &MoveRequest{
		From: PositionInfo{X: 4, Y: 6},
		To:   PositionInfo{X: 4, Y: 7},
		Nari: false,
	})

	assert.Equal(t, conn.Emiter.(*TestEmiter).Msg, "error")
	assert.Equal(t, conn.Emiter.(*TestEmiter).Data.(*ErrorResponse).Msg, "Koma is waiting time")

	g, _ := conn.GetGame()
	SetMovableTime(g, 4, 6)

	Move(conn, &MoveRequest{
		From: PositionInfo{X: 4, Y: 6},
		To:   PositionInfo{X: 4, Y: 7},
		Nari: false,
	})

	assert.Equal(t, conn.Emiter.(*TestEmiter).Msg, "error")
	assert.Equal(t, conn.Emiter.(*TestEmiter).Data.(*ErrorResponse).Msg, "Posiiton not found")

	Move(conn, &MoveRequest{
		From: PositionInfo{X: 4, Y: 6},
		To:   PositionInfo{X: 4, Y: 5},
		Nari: false,
	})

	// 歩を動かす
	assert.Equal(t, conn.RoomEmiter.(*TestRoomEmiter).Msg, "move")
	res := conn.RoomEmiter.(*TestRoomEmiter).Data.(*MoveResponse)
	assert.Equal(t, res.Player.Id, player1.Id)
	assert.Equal(t, res.From.X, 4)
	assert.Equal(t, res.From.Y, 6)
	assert.Equal(t, res.To.X, 4)
	assert.Equal(t, res.To.Y, 5)
	assert.False(t, res.Captured)

	// 歩を動かし続ける
	g, _ = conn.GetGame()
	SetMovableTime(g, 4, 5)
	Move(conn, &MoveRequest{From: PositionInfo{X: 4, Y: 5}, To: PositionInfo{X: 4, Y: 4}, Nari: false})
	g, _ = conn.GetGame()
	SetMovableTime(g, 4, 4)
	Move(conn, &MoveRequest{From: PositionInfo{X: 4, Y: 4}, To: PositionInfo{X: 4, Y: 3}, Nari: false})
	g, _ = conn.GetGame()
	SetMovableTime(g, 4, 3)
	Move(conn, &MoveRequest{From: PositionInfo{X: 4, Y: 3}, To: PositionInfo{X: 4, Y: 2}, Nari: true})

	// 相手の歩が取れたことの確認
	res = conn.RoomEmiter.(*TestRoomEmiter).Data.(*MoveResponse)
	assert.Equal(t, conn.RoomEmiter.(*TestRoomEmiter).Msg, "move")
	assert.True(t, res.Captured)
	assert.Equal(t, res.Tegoma[len(res.Tegoma)-1].Player.Id, player1.Id)
	assert.Equal(t, res.Tegoma[len(res.Tegoma)-1].KomaType, uint8(8), "歩が取れた")
	assert.Equal(t, res.RemovedKoma.KomaBase.KomaType, uint8(8), "歩が盤上から取り除かれた")
	assert.Equal(t, res.MovedKoma.KomaBase.KomaType, uint8(14), "と金に成った")

	// と金が相手の玉にとられる
	g, _ = conn.GetGame()
	SetMovableTime(g, 4, 0)
	Move(conn2, &MoveRequest{From: PositionInfo{X: 4, Y: 0}, To: PositionInfo{X: 4, Y: 1}, Nari: false})
	res = conn.RoomEmiter.(*TestRoomEmiter).Data.(*MoveResponse)
	g, _ = conn.GetGame()
	SetMovableTime(g, 4, 1)
	Move(conn2, &MoveRequest{From: PositionInfo{X: 4, Y: 1}, To: PositionInfo{X: 4, Y: 2}, Nari: false})

	// と金が相手の玉に取られたことの確認
	res = conn2.RoomEmiter.(*TestRoomEmiter).Data.(*MoveResponse)
	assert.Equal(t, conn2.RoomEmiter.(*TestRoomEmiter).Msg, "move")
	assert.True(t, res.Captured)
	assert.Equal(t, res.Tegoma[len(res.Tegoma)-1].Player.Id, player2.Id)
	assert.Equal(t, res.Tegoma[len(res.Tegoma)-1].KomaType, uint8(8), "歩が取れた")
	assert.Equal(t, res.RemovedKoma.KomaBase.KomaType, uint8(14), "と金が盤上から取り除かれた")
	assert.Equal(t, res.MovedKoma.KomaBase.KomaType, uint8(1), "玉が動いた")

	// 飛車で相手の玉を取る
	g, _ = conn.GetGame()
	SetMovableTime(g, 1, 7)
	Move(conn, &MoveRequest{From: PositionInfo{X: 1, Y: 7}, To: PositionInfo{X: 4, Y: 7}, Nari: false})
	g, _ = conn.GetGame()
	SetMovableTime(g, 4, 7)
	Move(conn, &MoveRequest{From: PositionInfo{X: 4, Y: 7}, To: PositionInfo{X: 4, Y: 2}, Nari: false})

	// 飛車で相手の玉を取れたことの確認
	res2 := conn.RoomEmiter.(*TestRoomEmiter).Data.(*FinishResponse)
	assert.Equal(t, conn.RoomEmiter.(*TestRoomEmiter).Msg, "finish")
	assert.Equal(t, res2.Winner.Id, player1.Id)
}
