package handler

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestJoin(t *testing.T) {
	// 一人目作成
	conn := NewTestContexts()

	roomid := "jointest"

	// 一人目参加
	Join(conn, &JoinRequest{Name: roomid})

	assert.Equal(t, conn.Emiter.(*TestEmiter).Msg, "join", "send join message")
	assert.Nil(t, conn.RoomEmiter.(*TestRoomEmiter).Data, "メンバーが揃うまで初期化通知は発行されない")

	g, ok := conn.GetGame()
	assert.True(t, ok, "game created")
	assert.NotNil(t, g, "game created")
	assert.Equal(t, len(g.Shogi.Players()), 1, "player created")

	// 二人目作成
	conn2 := NewTestContexts()

	// 二人目参加
	Join(conn2, &JoinRequest{Name: roomid})

	assert.Equal(t, conn2.Emiter.(*TestEmiter).Msg, "join", "send join message")
	assert.Equal(t, conn2.RoomEmiter.(*TestRoomEmiter).Msg, "initialize", "send init message")
	assert.NotNil(t, conn2.RoomEmiter.(*TestRoomEmiter).Data, "メンバーが揃って初期化通知は発行された")

	// 三人目作成&参加
	conn3 := NewTestContexts()
	Join(conn3, &JoinRequest{Name: roomid})
	assert.Equal(t, conn3.Emiter.(*TestEmiter).Msg, "error", "return error message")
	assert.Equal(t, conn3.Emiter.(*TestEmiter).Data.(*ErrorResponse).Msg, "too many player", "return valid error")
}
