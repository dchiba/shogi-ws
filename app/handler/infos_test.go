package handler

import (
	"bitbucket.org/dchiba/shogi-ws/app/model/game"
	"bitbucket.org/dchiba/shogi-ws/app/model/playerid"
	"bitbucket.org/dchiba/shogi/player"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewStateInfo(t *testing.T) {
	g := game.NewGame(game.Int2Mode(0))
	g.SetPlayer(player.New(playerid.NewPlayerId(), true))
	g.SetPlayer(player.New(playerid.NewPlayerId(), true))
	g.Shogi.Initialize()

	s := NewStateInfo(g)
	assert.NotNil(t, s)
}
