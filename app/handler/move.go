package handler

import (
	"bitbucket.org/dchiba/shogi-ws/app/connection"
	"bitbucket.org/dchiba/shogi/shogi"
)

func Move(conn *connection.Connection, data *MoveRequest) {
	g, ok := conn.GetGame()
	if !ok {
		conn.Emit("error", &ErrorResponse{Msg: "game is not exist"})
	}
	p := conn.Player
	b := g.Shogi.Ban
	from, err := b.NewPosition(data.From.X, data.From.Y)
	if err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}
	to, err := b.NewPosition(data.To.X, data.To.Y)
	if err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}
	moved, got, _, err := g.Shogi.Move(p, from, to, data.Nari)
	if err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}
	res := &MoveResponse{
		From:      data.From,
		To:        data.To,
		Player:    NewPlayerInfo(p),
		MovedKoma: NewKomaInfo(moved, to.X, to.Y),
		Tegoma:    []KomaBaseInfo{},
	}
	if got != nil {
		res.Captured = true
		res.RemovedKoma = NewKomaInfo(got, to.X, to.Y)
		for _, k := range p.Tegoma {
			res.Tegoma = append(res.Tegoma, NewKomaBaseInfo(k, p))
		}
	}
	conn.RoomEmit("move", res)

	if g.Status() == shogi.Finished {
		conn.RoomEmit("finish", &FinishResponse{Winner: NewPlayerInfo(g.Shogi.Winner)})
	}
}
