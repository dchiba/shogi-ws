package handler

import (
	"bitbucket.org/dchiba/shogi-ws/app/connection"
	"bitbucket.org/dchiba/shogi/koma"
)

func PutKoma(conn *connection.Connection, data *PutKomaRequest) {
	g, ok := conn.GetGame()
	if !ok {
		conn.Emit("error", &ErrorResponse{Msg: "game is not exist"})
	}
	p := conn.Player
	b := g.Shogi.Ban
	pos, err := b.NewPosition(data.Position.X, data.Position.Y)
	if err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}
	k := koma.New(data.Koma.Id, koma.Type(data.Koma.KomaType))
	newKoma, err := g.Shogi.PutKoma(p, k, pos)
	if err != nil {
		conn.Emit("error", &ErrorResponse{Msg: err.Error()})
		return
	}
	res := &PutKomaResponse{
		Position: data.Position,
		Player:   NewPlayerInfo(p),
		Koma:     NewKomaInfo(newKoma, data.Position.X, data.Position.Y),
		Tegoma:   []KomaBaseInfo{},
	}
	tegoma := g.Shogi.Player1.Tegoma
	if p.ID != g.Shogi.Player1.ID {
		tegoma = g.Shogi.Player2.Tegoma
	}
	for _, k := range tegoma {
		res.Tegoma = append(res.Tegoma, NewKomaBaseInfo(k, p))
	}
	conn.RoomEmit("put_koma", res)
}
