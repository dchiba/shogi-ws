package handler

import (
	"bitbucket.org/dchiba/shogi-ws/app/connection"
	"bitbucket.org/dchiba/shogi-ws/app/model/game"
	"bitbucket.org/dchiba/shogi/ban"
	"github.com/trevex/golem"
	"time"
)

type TestEmiter struct {
	Msg  string
	Data interface{}
}

func (e *TestEmiter) Emit(msg string, data interface{}) {
	e.Msg = msg
	e.Data = data
}

type TestRoomEmiter struct {
	Room string
	Msg  string
	Data interface{}
}

func (re *TestRoomEmiter) Emit(name string, msg string, data interface{}) {
	re.Room = name
	re.Msg = msg
	re.Data = data
}

func NewTestContexts() *connection.Connection {
	conn := connection.NewConnection(&golem.Connection{})
	conn.Emiter = &TestEmiter{}
	conn.RoomEmiter = &TestRoomEmiter{}
	return conn
}

func SetMovableTime(g *game.Game, x, y int) {
	k, _ := g.Shogi.Ban.GetKoma(&ban.Position{X: x, Y: y})
	g.Shogi.SetApptime(time.Unix(k.MovableTime(), 0))
}
