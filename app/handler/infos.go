package handler

import (
	"bitbucket.org/dchiba/shogi-ws/app/model/game"
	"bitbucket.org/dchiba/shogi/ban"
	"bitbucket.org/dchiba/shogi/koma"
	"bitbucket.org/dchiba/shogi/player"
)

// RandomJoin
type RandomJoinRequest struct {
	Mode int `json:"mode"`
}

// Join

type JoinRequest struct {
	Name string `json:"name"`
	Mode int    `json:"mode"`
}

type JoinResponse struct {
	Player PlayerInfo `json:"player"`
}

// Empty

type EmptyRequest struct{}

// Initialize

type InitializeResponse struct {
	Mode    int          `json:"mode"`
	Width   int          `json:"width"`
	Height  int          `json:"height"`
	Players []PlayerInfo `json:"players"`
	State   StateInfo    `json:"state"`
}

// Start

type StartResponse struct {
	CurrentTime int64 `json:"current_time"`
	StartTime   int64 `json:"start_time"`
}

// MovablePositions

type MovablePositionsRequest struct {
	Position PositionInfo `json:"position"`
}

type MovablePositionResponse struct {
	Positions []MovePositionInfo `json:"positions"`
}

// Move

type MoveRequest struct {
	From PositionInfo `json:"from"`
	To   PositionInfo `json:"to"`
	Nari bool         `json:"nari"`
}

type MoveResponse struct {
	Player      PlayerInfo     `json:"player"`
	From        PositionInfo   `json:"from"`
	To          PositionInfo   `json:"to"`
	MovedKoma   KomaInfo       `json:"moved_koma"`
	Captured    bool           `json:"captured"`
	RemovedKoma KomaInfo       `json:"removed_koma"`
	Tegoma      []KomaBaseInfo `json:"tegoma"`
}

// PutKoma

type PutKomaRequest struct {
	Position PositionInfo `json:"position"`
	Koma     KomaBaseInfo `json:"koma"`
}

type PutKomaResponse struct {
	Player   PlayerInfo     `json:"player"`
	Position PositionInfo   `json:"position"`
	Koma     KomaInfo       `json:"koma"`
	Tegoma   []KomaBaseInfo `json:"tegoma"`
}

// Finish

type FinishResponse struct {
	Winner PlayerInfo `json:"winner"`
}

// DebugSetTime

type DebugSetTimeRequest struct {
	Time int64 `json:"time"`
}

// Error

type ErrorResponse struct {
	Msg string `json:"msg"`
}

// Leave

type LeaveResponse struct {
	Player PlayerInfo `json:"player"`
}

// Infos

type PlayerInfo struct {
	Id uint64 `json:"id"`
}

func NewPlayerInfo(p *player.Player) PlayerInfo {
	return PlayerInfo{Id: p.ID}
}

type KomaBaseInfo struct {
	Id       uint8      `json:"id"`
	KomaType uint8      `json:"koma_type"`
	Player   PlayerInfo `json:"player"`
}

type KomaInfo struct {
	KomaBase      KomaBaseInfo `json:"koma_base"`
	LastMovedTime int64        `json:"last_moved_time"`
	MovableTime   int64        `json:"movable_time"`
	Position      PositionInfo `json:"position"`
}

type PositionInfo struct {
	X int `json:"x"`
	Y int `json:"y"`
}

type MovePositionInfo struct {
	Position PositionInfo `json:"position"`
	Nari     bool         `json:"nari"`
}

type StateInfo struct {
	Ban     []KomaInfo     `json:"ban"`
	Tegoma1 []KomaBaseInfo `json:"tegoma1"`
	Tegoma2 []KomaBaseInfo `json:"tegoma2"`
}

func NewStateInfo(g *game.Game) StateInfo {
	s := StateInfo{
		Ban:     []KomaInfo{},
		Tegoma1: []KomaBaseInfo{},
		Tegoma2: []KomaBaseInfo{},
	}
	for y, row := range g.Shogi.Ban.Table {
		for x, col := range row {
			if col == nil {
				continue
			}
			s.Ban = append(s.Ban, NewKomaInfo(col, x, y))
		}
	}
	for _, k := range g.Shogi.Player1.Tegoma {
		s.Tegoma1 = append(s.Tegoma1, NewKomaBaseInfo(k, g.Shogi.Player1))
	}
	for _, k := range g.Shogi.Player2.Tegoma {
		s.Tegoma2 = append(s.Tegoma2, NewKomaBaseInfo(k, g.Shogi.Player2))
	}
	return s
}

func NewKomaInfo(k *ban.Koma, x, y int) KomaInfo {
	return KomaInfo{
		KomaBase:      NewKomaBaseInfo(&k.Koma, k.Player),
		LastMovedTime: k.LastMoveUnixtime,
		MovableTime:   k.MovableTime(),
		Position:      PositionInfo{X: x, Y: y},
	}
}

func NewKomaBaseInfo(k *koma.Koma, p *player.Player) KomaBaseInfo {
	return KomaBaseInfo{
		Id:       k.ID,
		KomaType: uint8(k.Type),
		Player:   NewPlayerInfo(p),
	}
}
