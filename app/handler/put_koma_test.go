package handler

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPutKoma(t *testing.T) {
	roomid := "putkomatest"

	// プレイヤー参加
	conn := NewTestContexts()
	Join(conn, &JoinRequest{Name: roomid})
	conn2 := NewTestContexts()
	Join(conn2, &JoinRequest{Name: roomid})

	// 初期化終了
	Initialized(conn, &EmptyRequest{})
	Initialized(conn2, &EmptyRequest{})

	// 歩で相手の歩を取る
	g, _ := conn.GetGame()
	SetMovableTime(g, 4, 6)
	Move(conn, &MoveRequest{From: PositionInfo{X: 4, Y: 6}, To: PositionInfo{X: 4, Y: 5}, Nari: false})
	g, _ = conn.GetGame()
	SetMovableTime(g, 4, 5)
	Move(conn, &MoveRequest{From: PositionInfo{X: 4, Y: 5}, To: PositionInfo{X: 4, Y: 4}, Nari: false})
	g, _ = conn.GetGame()
	SetMovableTime(g, 4, 4)
	Move(conn, &MoveRequest{From: PositionInfo{X: 4, Y: 4}, To: PositionInfo{X: 4, Y: 3}, Nari: false})
	g, _ = conn.GetGame()
	SetMovableTime(g, 4, 3)
	Move(conn, &MoveRequest{From: PositionInfo{X: 4, Y: 3}, To: PositionInfo{X: 4, Y: 2}, Nari: true})

	p := g.Shogi.Player1

	// 歩を打つ（駒がある場所なのでおけない）
	PutKoma(conn, &PutKomaRequest{
		Position: PositionInfo{X: 5, Y: 6},
		Koma:     NewKomaBaseInfo(p.Tegoma[0], p),
	})

	assert.Equal(t, conn.Emiter.(*TestEmiter).Msg, "error")
	assert.Equal(t, conn.Emiter.(*TestEmiter).Data.(*ErrorResponse).Msg, "Koma is exist")

	// 歩を打つ
	PutKoma(conn, &PutKomaRequest{
		Position: PositionInfo{X: 4, Y: 6},
		Koma:     NewKomaBaseInfo(p.Tegoma[0], p),
	})

	// 歩が置けたことの確認
	assert.Equal(t, conn.RoomEmiter.(*TestRoomEmiter).Msg, "put_koma")
	res := conn.RoomEmiter.(*TestRoomEmiter).Data.(*PutKomaResponse)
	assert.Equal(t, res.Player.Id, p.ID)
	assert.Equal(t, res.Position.X, 4)
	assert.Equal(t, res.Position.Y, 6)
	assert.Equal(t, res.Koma.KomaBase.Player.Id, p.ID)
	assert.Equal(t, res.Koma.KomaBase.KomaType, uint8(8), "歩を置いた")
	assert.Equal(t, len(p.Tegoma), 0, "手持ちの駒がない")
}
