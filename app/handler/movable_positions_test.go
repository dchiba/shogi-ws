package handler

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMovablePositions(t *testing.T) {
	roomid := "movablepositionstest"

	// プレイヤー参加
	conn := NewTestContexts()
	Join(conn, &JoinRequest{Name: roomid})
	conn2 := NewTestContexts()
	Join(conn2, &JoinRequest{Name: roomid})

	// 初期化終了
	Initialized(conn, &EmptyRequest{})
	Initialized(conn2, &EmptyRequest{})

	MovablePositions(conn, &MovablePositionsRequest{Position: PositionInfo{X: 4, Y: 0}})
	assert.Equal(t, conn.Emiter.(*TestEmiter).Msg, "error")
	assert.Equal(t, conn.Emiter.(*TestEmiter).Data.(*ErrorResponse).Msg, "Koma is not owned")

	MovablePositions(conn, &MovablePositionsRequest{Position: PositionInfo{X: 4, Y: 8}})
	assert.Equal(t, conn.Emiter.(*TestEmiter).Msg, "movable_positions")
	res := conn.Emiter.(*TestEmiter).Data.(*MovablePositionResponse)
	assert.Equal(t, len(res.Positions), 3)
}
