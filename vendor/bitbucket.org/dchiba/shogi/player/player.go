package player

import (
	"bitbucket.org/dchiba/shogi/koma"
	"errors"
)

type Player struct {
	ID     uint64
	Sente  bool
	Tegoma []*koma.Koma
}

func New(id uint64, sente bool) *Player {
	return &Player{ID: id, Sente: sente, Tegoma: []*koma.Koma{}}
}

func (p *Player) AppendTegoma(k *koma.Koma) (*koma.Koma, error) {
	// 重複チェック
	for _, val := range p.Tegoma {
		if k.ID == val.ID {
			return nil, errors.New("duplicated")
		}
	}
	// 成り駒をもどす
	ret := koma.New(k.ID, k.Type)
	for key, val := range koma.NariMap {
		if k.Type == val {
			ret.Type = key
		}
	}
	p.Tegoma = append(p.Tegoma, ret)
	return ret, nil
}

func (p *Player) HasTegoma(k *koma.Koma) bool {
	for _, val := range p.Tegoma {
		if k.ID == val.ID {
			return true
		}
	}
	return false
}

func (p *Player) RemoveTegoma(k *koma.Koma) (*koma.Koma, error) {
	for i, val := range p.Tegoma {
		if k.ID == val.ID {
			p.Tegoma = append(p.Tegoma[:i], p.Tegoma[i+1:]...)
			return k, nil
		}
	}
	return nil, errors.New("Kom is not found")
}
