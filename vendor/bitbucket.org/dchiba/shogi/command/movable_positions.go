package command

import (
	"bitbucket.org/dchiba/shogi/ban"
	"bitbucket.org/dchiba/shogi/koma"
	"bitbucket.org/dchiba/shogi/player"
	"errors"
	"fmt"
)

var (
	KomaNotFoundError     error = errors.New("Koma is not found")
	KomaNotOwnedError     error = errors.New("Koma is not owned")
	PositionNotFoundError error = errors.New("Posiiton not found")
	TargetKomaOwnedError  error = errors.New("Target Koma is mine")
)

type MoveFunction func(*ban.Position, bool) (*ban.Position, error)

type MovePosition struct {
	Position *ban.Position
	Nari     bool
}

func NewMovePosition(b *ban.Ban, k *ban.Koma, from *ban.Position, to *ban.Position) *MovePosition {
	p := k.Player
	mp := &MovePosition{to, false}
	senteNariHeight := b.NariHeight - 1
	gotenNariHeight := b.Height - b.NariHeight
	if k.Type.CanNari() {
		if p.Sente {
			if from.Y <= senteNariHeight || to.Y <= senteNariHeight {
				mp.Nari = true
			}
		} else {
			if from.Y >= gotenNariHeight || to.Y >= gotenNariHeight {
				mp.Nari = true
			}
		}
	}
	return mp
}

func (p *MovePosition) String() string {
	return fmt.Sprintf("{X:%d Y:%d N:%t}", p.Position.X, p.Position.Y, p.Nari)
}

type MovePositions []*MovePosition

func (ps MovePositions) Contain(q *ban.Position) (b bool) {
	b = false
	for _, p := range ps {
		if p.Position.Equal(q) {
			b = true
			break
		}
	}
	return
}

func MovablePositions(p *player.Player, b *ban.Ban, pos *ban.Position) (MovePositions, error) {
	k, e := b.GetKoma(pos)
	if e != nil {
		return nil, KomaNotFoundError
	}
	if k.Player.ID != p.ID {
		return nil, KomaNotOwnedError
	}

	positions := MovePositions{}

	for _, m := range k.GetMoveTypes() {
		var ps MovePositions
		var e error

		switch m {
		case koma.Front:
			ps, e = FrontPositions(b, k, pos)
		case koma.Back:
			ps, e = BackPositions(b, k, pos)
		case koma.Right:
			ps, e = RightPositions(b, k, pos)
		case koma.Left:
			ps, e = LeftPositions(b, k, pos)
		case koma.FrontRight:
			ps, e = FrontRightPositions(b, k, pos)
		case koma.FrontLeft:
			ps, e = FrontLeftPositions(b, k, pos)
		case koma.BackRight:
			ps, e = BackRightPositions(b, k, pos)
		case koma.BackLeft:
			ps, e = BackLeftPositions(b, k, pos)
		case koma.FrontStraight:
			ps, e = FrontStraightPositions(b, k, pos)
		case koma.BackStraight:
			ps, e = BackStraightPositions(b, k, pos)
		case koma.RightStraight:
			ps, e = RightStraightPositions(b, k, pos)
		case koma.LeftStraight:
			ps, e = LeftStraightPositions(b, k, pos)
		case koma.FrontRightStraight:
			ps, e = FrontRightStraightPositions(b, k, pos)
		case koma.FrontLeftStraight:
			ps, e = FrontLeftLeftStraightPositions(b, k, pos)
		case koma.BackRightStraight:
			ps, e = BackRightStraightPositions(b, k, pos)
		case koma.BackLeftStraight:
			ps, e = BackLeftStraightPositions(b, k, pos)
		case koma.FrontFrontRightJump:
			ps, e = FrontFrontRightJumpPositions(b, k, pos)
		case koma.FrontFrontLeftJump:
			ps, e = FrontFrontLeftJumpPositions(b, k, pos)
		}

		if e == nil {
			positions = append(positions, ps...)
		}
	}

	return positions, nil
}

func FrontPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Front)
}

func BackPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Back)
}

func RightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Right)
}

func LeftPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Left)
}

func FrontRightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.FrontRight)
}

func FrontLeftPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.FrontLeft)
}

func BackRightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.BackRight)
}

func BackLeftPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.BackLeft)
}

func FrontStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Front, b.Front, b.Front, b.Front, b.Front, b.Front, b.Front, b.Front)
}

func BackStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Back, b.Back, b.Back, b.Back, b.Back, b.Back, b.Back, b.Back)
}

func RightStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Right, b.Right, b.Right, b.Right, b.Right, b.Right, b.Right, b.Right)
}

func LeftStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Left, b.Left, b.Left, b.Left, b.Left, b.Left, b.Left, b.Left)
}

func FrontRightStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.FrontRight, b.FrontRight, b.FrontRight, b.FrontRight, b.FrontRight, b.FrontRight, b.FrontRight, b.FrontRight)
}

func FrontLeftLeftStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.FrontLeft, b.FrontLeft, b.FrontLeft, b.FrontLeft, b.FrontLeft, b.FrontLeft, b.FrontLeft, b.FrontLeft)
}

func BackRightStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.BackRight, b.BackRight, b.BackRight, b.BackRight, b.BackRight, b.BackRight, b.BackRight, b.BackRight)
}

func BackLeftStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.BackLeft, b.BackLeft, b.BackLeft, b.BackLeft, b.BackLeft, b.BackLeft, b.BackLeft, b.BackLeft)
}

func FrontFrontRightJumpPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, true, b.Front, b.Front, b.Right)
}

func FrontFrontLeftJumpPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, true, b.Front, b.Front, b.Left)
}

func GetPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position, jump bool, funcs ...MoveFunction) (MovePositions, error) {
	positions := MovePositions{}
	before := pos
	last := len(funcs) - 1
	for i, f := range funcs {
		dest, e := f(before, k.Player.Sente)
		if e != nil {
			// Position が取得できない
			break
		}
		before = dest

		// ジャンプの途中なら次の移動へ
		if jump && i != last {
			continue
		}

		target, e := b.GetKoma(dest)
		if e == nil {
			// 駒がある
			if target.Player.ID != k.Player.ID {
				// 相手の駒
				positions = append(positions, NewMovePosition(b, k, pos, dest))
			}
			break
		}
		positions = append(positions, NewMovePosition(b, k, pos, dest))
	}
	if len(positions) <= 0 {
		return nil, PositionNotFoundError
	}
	return positions, nil
}
