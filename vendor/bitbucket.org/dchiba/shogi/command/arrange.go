package command

import (
	"bitbucket.org/dchiba/shogi/ban"
	"bitbucket.org/dchiba/shogi/player"
	"bitbucket.org/dchiba/shogi/teaiwari"
)

func Arrange(p1 *player.Player, p2 *player.Player, b *ban.Ban, teaiwari teaiwari.Teaiwari) {
	for _, t := range teaiwari {
		var p *player.Player
		if t.Uwate {
			p = p1
		} else {
			p = p2
		}
		pos, e := b.NewPosition(t.X, t.Y)
		if e != nil {
			panic("invalid position")
		}
		b.SetKoma(pos, t.Type, p)
	}
}
