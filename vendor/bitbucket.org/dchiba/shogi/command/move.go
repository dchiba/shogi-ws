package command

import (
	"bitbucket.org/dchiba/shogi/ban"
	"bitbucket.org/dchiba/shogi/koma"
	"bitbucket.org/dchiba/shogi/player"
	"errors"
)

func Move(p *player.Player, b *ban.Ban, from *ban.Position, to *ban.Position, nari bool) (moved *ban.Koma, got *ban.Koma, captured *koma.Koma, e error) {
	// 駒があるか
	k, e := b.GetKoma(from)
	if e != nil {
		e = KomaNotFoundError
		return
	}
	if k.Player.ID != p.ID {
		e = KomaNotOwnedError
		return
	}

	// 移動可能かどうか
	if !k.IsMovableTime() {
		e = errors.New("Koma is waiting time")
		return
	}
	positions, e := MovablePositions(p, b, from)
	if e != nil {
		e = PositionNotFoundError
		return
	}
	contain := false
	canNari := false
	for _, mp := range positions {
		if mp.Position.Equal(to) {
			contain = true
			canNari = mp.Nari
		}
	}
	if !contain {
		e = PositionNotFoundError
		return
	}

	// 成れるかどうか
	if nari && (!canNari || !k.CanNari()) {
		e = errors.New("Koma is not able to NARI")
		return
	}

	// 移動
	got, e = b.MoveKoma(from, to)
	if e != nil {
		return
	}
	// 移動先に敵の駒があれば手駒にする
	if got != nil {
		captured, e = p.AppendTegoma(&got.Koma)
		if e != nil {
			return nil, nil, nil, e
		}
	}
	moved = k

	// 成る
	if nari {
		newType, err := k.Nari()
		if err != nil {
			e = err
			return
		}
		k.Type = newType
		moved = k
	}

	return
}
