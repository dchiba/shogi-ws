package command

import (
	"bitbucket.org/dchiba/shogi/ban"
	"bitbucket.org/dchiba/shogi/koma"
	"bitbucket.org/dchiba/shogi/player"
	"errors"
)

func PutKoma(p *player.Player, b *ban.Ban, k *koma.Koma, pos *ban.Position) (*ban.Koma, error) {
	if !p.HasTegoma(k) {
		return nil, errors.New("Koma is not found")
	}
	newKoma, e := b.PutKoma(p, k, pos)
	if e != nil {
		return nil, e
	}
	_, e = p.RemoveTegoma(k)
	if e != nil {
		return nil, e
	}
	return newKoma, nil
}
