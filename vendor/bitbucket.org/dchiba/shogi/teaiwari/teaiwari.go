package teaiwari

import "bitbucket.org/dchiba/shogi/koma"

type Position struct {
	X, Y  int
	Type  koma.Type
	Uwate bool
}

type Teaiwari []Position
