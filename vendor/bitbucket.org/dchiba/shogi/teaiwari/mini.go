package teaiwari

import (
	"bitbucket.org/dchiba/shogi/koma"
)

var Mini Teaiwari = Teaiwari{
	Position{0, 0, koma.GIN, true},
	Position{1, 0, koma.KIN, true},
	Position{2, 0, koma.GYOKU, true},
	Position{3, 0, koma.KIN, true},
	Position{4, 0, koma.GIN, true},

	Position{1, 2, koma.FU, true},
	Position{2, 2, koma.FU, true},
	Position{3, 2, koma.FU, true},

	Position{1, 3, koma.FU, false},
	Position{2, 3, koma.FU, false},
	Position{3, 3, koma.FU, false},

	Position{0, 5, koma.GIN, false},
	Position{1, 5, koma.KIN, false},
	Position{2, 5, koma.GYOKU, false},
	Position{3, 5, koma.KIN, false},
	Position{4, 5, koma.GIN, false},
}
