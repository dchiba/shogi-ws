package teaiwari

import (
	"bitbucket.org/dchiba/shogi/koma"
)

var Hirate Teaiwari = Teaiwari{
	Position{0, 0, koma.KYOU, true},
	Position{1, 0, koma.KEI, true},
	Position{2, 0, koma.GIN, true},
	Position{3, 0, koma.KIN, true},
	Position{4, 0, koma.GYOKU, true},
	Position{5, 0, koma.KIN, true},
	Position{6, 0, koma.GIN, true},
	Position{7, 0, koma.KEI, true},
	Position{8, 0, koma.KYOU, true},

	Position{1, 1, koma.KAKU, true},
	Position{7, 1, koma.HISYA, true},

	Position{0, 2, koma.FU, true},
	Position{1, 2, koma.FU, true},
	Position{2, 2, koma.FU, true},
	Position{3, 2, koma.FU, true},
	Position{4, 2, koma.FU, true},
	Position{5, 2, koma.FU, true},
	Position{6, 2, koma.FU, true},
	Position{7, 2, koma.FU, true},
	Position{8, 2, koma.FU, true},

	Position{0, 6, koma.FU, false},
	Position{1, 6, koma.FU, false},
	Position{2, 6, koma.FU, false},
	Position{3, 6, koma.FU, false},
	Position{4, 6, koma.FU, false},
	Position{5, 6, koma.FU, false},
	Position{6, 6, koma.FU, false},
	Position{7, 6, koma.FU, false},
	Position{8, 6, koma.FU, false},

	Position{1, 7, koma.HISYA, false},
	Position{7, 7, koma.KAKU, false},

	Position{0, 8, koma.KYOU, false},
	Position{1, 8, koma.KEI, false},
	Position{2, 8, koma.GIN, false},
	Position{3, 8, koma.KIN, false},
	Position{4, 8, koma.GYOKU, false},
	Position{5, 8, koma.KIN, false},
	Position{6, 8, koma.GIN, false},
	Position{7, 8, koma.KEI, false},
	Position{8, 8, koma.KYOU, false},
}
