package shogi

import (
	myapptime "bitbucket.org/dchiba/shogi/apptime"
	"bitbucket.org/dchiba/shogi/ban"
	"bitbucket.org/dchiba/shogi/command"
	"bitbucket.org/dchiba/shogi/koma"
	"bitbucket.org/dchiba/shogi/player"
	"bitbucket.org/dchiba/shogi/teaiwari"
	"errors"
	"github.com/nirasan/apptime"
	"time"
)

type Shogi struct {
	Mode    Mode
	Player1 *player.Player
	Player2 *player.Player
	Ban     *ban.Ban
	Status  Status
	Winner  *player.Player
}

type Mode int8

const (
	DefaultMode Mode = iota
	MiniMode
	MicroMode
)

type Status int8

const (
	Preparing Status = iota
	Playing
	Finished
)

var (
	NotPlaying    = errors.New("Shogi is not playing")
	InvalidPlayer = errors.New("Invalid player")
)

func New(mode Mode) *Shogi {
	var b *ban.Ban
	switch mode {
	case DefaultMode:
		b = ban.NewDefaultBan()
	case MiniMode:
		b = ban.NewMiniBan()
	case MicroMode:
		b = ban.NewMicroBan()
	default:
		panic("invalid mode")
	}
	return &Shogi{
		Mode:   mode,
		Ban:    b,
		Status: Preparing,
	}
}

func (s *Shogi) Players() []*player.Player {
	players := []*player.Player{}
	if s.Player1 != nil {
		players = append(players, s.Player1)
	}
	if s.Player2 != nil {
		players = append(players, s.Player2)
	}
	return players
}

func (s *Shogi) SetPlayer(p *player.Player) error {
	if s.Player1 == nil {
		s.Player1 = p
	} else if s.Player2 == nil {
		s.Player2 = p
	} else {
		return errors.New("too many player")
	}
	return nil
}

func (s *Shogi) HasPlayer(p *player.Player) bool {
	return s.Player1 == p || s.Player2 == p
}

func (s *Shogi) IsPlaying() bool {
	return s.Status == Playing
}

func (s *Shogi) Prepared() bool {
	return s.Status == Preparing && s.Player1 != nil && s.Player2 != nil
}

func (s *Shogi) Initialize() {
	if s.Prepared() {
		s.Status = Playing
		var t teaiwari.Teaiwari
		switch s.Mode {
		case DefaultMode:
			t = teaiwari.Hirate
		case MiniMode:
			t = teaiwari.Mini
		case MicroMode:
			t = teaiwari.Micro
		default:
			panic("invalid mode")
		}
		command.Arrange(s.Player2, s.Player1, s.Ban, t)
	} else {
		panic("invalid status")
	}
}

func (s *Shogi) MovablePositions(p *player.Player, pos *ban.Position) (command.MovePositions, error) {
	if !s.IsPlaying() {
		return nil, NotPlaying
	}
	if !s.HasPlayer(p) {
		return nil, InvalidPlayer
	}
	return command.MovablePositions(p, s.Ban, pos)
}

func (s *Shogi) PutKoma(p *player.Player, k *koma.Koma, pos *ban.Position) (*ban.Koma, error) {
	if !s.IsPlaying() {
		return nil, NotPlaying
	}
	if !s.HasPlayer(p) {
		return nil, InvalidPlayer
	}
	return command.PutKoma(p, s.Ban, k, pos)
}

func (s *Shogi) Move(p *player.Player, from *ban.Position, to *ban.Position, nari bool) (*ban.Koma, *ban.Koma, *koma.Koma, error) {
	if !s.IsPlaying() {
		return nil, nil, nil, NotPlaying
	}
	if !s.HasPlayer(p) {
		return nil, nil, nil, InvalidPlayer
	}
	moved, got, captured, err := command.Move(p, s.Ban, from, to, nari)
	if got != nil && got.Type == koma.GYOKU {
		s.Winner = p
		s.Status = Finished
	}
	return moved, got, captured, err
}

func (s *Shogi) GetApptime() *apptime.Apptime {
	return myapptime.GetTime()
}

func (s *Shogi) SetApptime(t time.Time) {
	myapptime.SetTime(t)
}

func (s *Shogi) ClearApptime() {
	myapptime.Clear()
}
