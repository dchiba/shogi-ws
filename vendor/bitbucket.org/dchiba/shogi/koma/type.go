package koma

import "errors"

type Type uint8

const (
	_ Type = iota
	GYOKU
	HISYA
	KAKU
	KIN
	GIN
	KEI
	KYOU
	FU
	RYU
	UMA
	NARIGIN
	NARIKEI
	NARIKYOU
	TOKIN

	ZOU
	KIRIN
)

var NariMap map[Type]Type = map[Type]Type{
	HISYA: RYU,
	KAKU:  UMA,
	GIN:   NARIGIN,
	KEI:   NARIKEI,
	KYOU:  NARIKYOU,
	FU:    TOKIN,
}

var LabelList = [...]string{
	"",
	"玉", "飛", "角", "金", "銀", "桂", "香", "歩",
	"竜", "馬", "全", "圭", "杏", "と",
	"象", "麒",
}

var WaitTimeList = [...]int64{
	0, // _ Type = iota
	7, // GYOKU
	7, // HISYA
	7, // KAKU
	5, // KIN
	5, // GIN
	3, // KEI
	3, // KYOU
	3, // FU
	8, // RYU
	8, // UMA
	5, // NARIGIN
	5, // NARIKEI
	5, // NARIKYOU
	5, // TOKIN
	5, // ZOU
	5, // KIRIN
}

func (t Type) CanNari() bool {
	_, ok := NariMap[t]
	return ok
}

func (t Type) Nari() (rt Type, e error) {
	rt, ok := NariMap[t]
	if !ok {
		e = errors.New("invalid type")
	}
	return
}

func (t Type) Label() string {
	return LabelList[t]
}
