package main

import (
	"bitbucket.org/dchiba/shogi-ws/app/connection"
	"bitbucket.org/dchiba/shogi-ws/app/handler"
	"fmt"
	"github.com/trevex/golem"
	"net/http"
	"os"
)

func onConnect(conn *connection.Connection, r *http.Request) {
	fmt.Println("run onConnect")
}
func onClose(conn *connection.Connection) {
	fmt.Println("run onClose")
	conn.LeaveAll()
	// 退室の通知
	if _, ok := conn.GetGame(); ok {
		conn.RoomEmit("leave", &handler.LeaveResponse{Player: handler.NewPlayerInfo(conn.Player)})
	}
	// RandomJoin 待ち中ならクリア
	if conn.RandomJoin != connection.RandomJoinDefault {
		handler.RandomJoinClear(conn, conn.RandomJoin)
	}
}

func main() {
	http.HandleFunc("/ws", createRouter().Handler())
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	http.ListenAndServe(":"+port, nil)
}

func createRouter() *golem.Router {
	myrouter := golem.NewRouter()
	myrouter.SetConnectionExtension(connection.NewConnection)
	myrouter.OnConnect(onConnect)
	myrouter.On("random_join", handler.RandomJoin)
	myrouter.On("join", handler.Join)
	myrouter.On("initialized", handler.Initialized)
	myrouter.On("movable_positions", handler.MovablePositions)
	myrouter.On("move", handler.Move)
	myrouter.On("put_koma", handler.PutKoma)
	myrouter.On("debug_set_time", handler.DebugSetTime)
	myrouter.OnClose(onClose)
	return myrouter
}
