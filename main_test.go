package main

import (
	"errors"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

func TestValidCase(t *testing.T) {
	// サーバーの作成
	ts := httptest.NewServer(http.HandlerFunc(createRouter().Handler()))
	defer ts.Close()

	// クライアント1の作成
	client1, err := createClient(ts)
	if err != nil {
		t.Fatal(err)
	}
	defer client1.Close()

	// クライアント2の作成
	client2, err := createClient(ts)
	if err != nil {
		t.Fatal(err)
	}
	defer client2.Close()

	// クライアント1がルームに参加
	err = writeMessage(client1, `join {"name":"room1","mode":0}`)
	if err != nil {
		t.Fatal(err)
	}

	// JoinResponse
	ev, data, err := readMessage(client1)
	if err != nil {
		t.Error(err)
	}
	if ev != "join" {
		t.Error("response is not valid: " + ev + " " + data)
	}

	// クライアント2がルームに参加
	err = writeMessage(client2, `join {"name":"room1","mode":0}`)
	if err != nil {
		t.Fatal(err)
	}

	// JoinResponse
	ev, data, err = readMessage(client2)
	if err != nil {
		t.Error(err)
	}
	if ev != "join" {
		t.Error("response is not valid: " + ev + " " + data)
	}

	time.Sleep(1 * time.Second)

	// Initialize
	ev, data, err = readMessage(client1)
	if ev != "initialize" {
		t.Error("response is not valid: " + ev + " " + data)
	}
	ev, data, err = readMessage(client2)
	if ev != "initialize" {
		t.Error("response is not valid: " + ev + " " + data)
	}

	// Initialized
	err = writeMessage(client1, `initialized {}`)
	if err != nil {
		t.Fatal(err)
	}
	err = writeMessage(client2, `initialized {}`)
	if err != nil {
		t.Fatal(err)
	}

	// Start
	ev, data, err = readMessage(client1)
	if ev != "start" {
		t.Error("response is not valid: " + ev + " " + data)
	}
	ev, data, err = readMessage(client2)
	if ev != "start" {
		t.Error("response is not valid: " + ev + " " + data)
	}

	time.Sleep(2 * time.Second)

	// MovablePositions
	err = writeMessage(client1, `movable_positions {"position":{"x":0,"y":6}}`)
	if err != nil {
		t.Fatal(err)
	}
	ev, data, err = readMessage(client1)
	if ev != "movable_positions" {
		t.Error("response is not valid: " + ev + " " + data)
	}
}

func createClient(ts *httptest.Server) (*websocket.Conn, error) {
	dialer := websocket.Dialer{
		Subprotocols:    []string{},
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}

	url := strings.Replace(ts.URL, "http://", "ws://", 1)
	header := http.Header{"Accept-Encoding": []string{"gzip"}}

	conn, _, err := dialer.Dial(url, header)
	if err != nil {
		return nil, err
	}

	return conn, nil
}

func writeMessage(conn *websocket.Conn, message string) error {
	return conn.WriteMessage(websocket.TextMessage, []byte(message))
}

func readMessage(conn *websocket.Conn) (string, string, error) {
	conn.SetReadDeadline(time.Now().Add(1 * time.Second))
	messageType, p, err := conn.ReadMessage()
	if err != nil {
		return "", "", err
	}
	if messageType != websocket.TextMessage {
		return "", "", errors.New("invalid message type")
	}
	messages := strings.SplitN(string(p), " ", 2)
	log.Println(messages)
	return messages[0], messages[1], nil
}
